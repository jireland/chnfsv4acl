# NAME
    chnfsv4acl - tool for creating and applying GPFS NFSv4 ACLs

# SYNOPSIS
    chnfsv4acl [-vhdnR] MODE[,MODE] [<directory|file> ...]

# DESCRIPTION
    chnfsv4acl is a tool for creating and applying GPFS NFSv4 ACLs to files
    and directories; either individually or recursively. The ACLS are
    specified according to a symbolic MODE, similar to that used in the
    chmod command, but modified to cope with the extended attributes
    available in GPFS NFSv4.

    In a GPFS NFSv6 ACL the second part of the first line of each stanza (or
    element: ACE) that makes up the ACL displays a 'rwxc' translation of the
    permissions that appear on the subsequent two lines. chnfsv4acl can be
    thought of as a tool for reverse engineering an ACL starting form an
    extended 'rwxc' string.

    For 'special' stanzas in the ACl, the format of a symbolic MODE is
    [aoge...][[+-=#][perms...]...] where perms is either zero or more
    letters from the set 'rwxcaDdfipI'. Multiple symbolic modes can be
    given, separated by commas.

    The special MODE "-" allows 'chnfsv4acl' to read an ACL from standard
    input and apply to all files and directories on the command line;
    possibly recursively. The ACL is read as a full NFSv4 ACL - not an ACL
    in MODE format. Mormally used with the 'mmgetacl' command.

    The operator + causes the permision bits to be added to the existing
    file stanza, - causes them to be removed, = causes them to changed, and
    # causes the whole stanza to be removed from the ACL.

    The 'perms' letters 'rwxcADdfipI' select file access, type, and flag
    bits: read (r), write (w), execute (x), control (c), allow (A), deny
    (D), DirInherit (d), FileInherit (f), InheritOnly (i),
    NoPropagateInherit (p), Inherited (I). In the case of the file mode bits
    (rwxc) the correct combination of 14 NFSv4 access bits is constructed
    and applied to the files specified on the command line.

    A combination of the letters oge controls which 'special' stanza will be
    changed: the owner of the file (o), other users in the file's group (g),
    everyone else (e). Note, this is different from chmod where 'ugoa'
    specify user, group, other, all. The 'a' specifies all ACEs - special,
    group, and user. The 'a' can be dropped and is implied if there is
    othing before an '=', '+', or '-'.

    Extended user and group stanzas can be constructed using the syntax,
    u:<user-name>[:<user-name>...][+-=#][perms...]...] and
    g:<group-name>[:<group-name>...][+-=#][perms...]...]

    With no files given on the command line, chnfsv4acl prints out the NFSv4
    ACL based on the mode given on the command line. This output can be
    piped into 'mmputacl' as an alternative way to change a files ACL.

    The '-R' option apples an ACL recursively to all files and directories
    inside a directory, correctly setting the subdirectories permissions
    based on the inheritance flags of the parent directory and switching on
    the 'Inherited' flag where appropriate.

    The -p (preserve) and -r (remove) options control how new MODEs are
    applied to files and directories specified on the command line. By
    default, the existing ACL is cleared before applying MODE.

# OPTIONS
    -   Read the ACL from the standard input.

    -h  print help

    -n  Apply a "NTFS ACL" style mapping to the 14 NFSv4 access bits. Allow
        the use of 'l' (list) and 'm' (modify) in the MODE specification.

    -p  Preserve ALL existing ACEs before appending the '=' permissions
        specified in MODE. Preserve is the default befavour if +/- is used
        in MODE. Otherwise, all ACEs are cleared before applying MODE.

    -r  Remove ACEs specified in MODE from the files and directories
        specified on the command line - if they are present - before
        appending the permissions in MODE.

    -I  Switch the 'Inherited' flag on in all ACEs of files and directories
        specified on the command line.

    -R  Apply permissions recursively, setting the subdirectory and
        subdirectory file permissions based on the inheritance flags of the
        parent directory.

# EXAMPLE
    To print the three stanza ACL associated with:

     OWNER Read-Write-Execute-Control-DirectoryInherit, 
     OWNER Read-Write-FileInherit, 
     EVERYONE NoAccessRights-FileInherit-DirectoryInherit

     $chnfsv4acl o=rwxcd,o=rwf,e=fd
     #NFSv4 ACL
     special:owner@:rwxc:allow:DirInherit
      (X)READ/LIST (X)WRITE/CREATE (X)APPEND/MKDIR (X)SYNCHRONIZE (X)READ_ACL  (X)READ_ATTR  (X)READ_NAMED
      (X)DELETE    (X)DELETE_CHILD (X)CHOWN        (X)EXEC/SEARCH (X)WRITE_ACL (X)WRITE_ATTR (X)WRITE_NAMED
 
     special:owner@:rw--:allow:FileInherit
      (X)READ/LIST (X)WRITE/CREATE (X)APPEND/MKDIR (X)SYNCHRONIZE (X)READ_ACL  (X)READ_ATTR  (X)READ_NAMED
      (-)DELETE    (X)DELETE_CHILD (-)CHOWN        (-)EXEC/SEARCH (-)WRITE_ACL (-)WRITE_ATTR (-)WRITE_NAMED
 
     special:everyone@:----:allow:FileInherit:DirInherit
      (-)READ/LIST (-)WRITE/CREATE (-)APPEND/MKDIR (-)SYNCHRONIZE (-)READ_ACL  (-)READ_ATTR  (-)READ_NAMED
      (-)DELETE    (-)DELETE_CHILD (-)CHOWN        (-)EXEC/SEARCH (-)WRITE_ACL (-)WRITE_ATTR (-)WRITE_NAMED

    To apply this recursively to the directory <directory>:

     chnfsv4acl -R o=rwxdc,o=rwf,e=fd <directory>

    To include Read-Write-Execute-FileInherit-DirectoryInherit for the group
    'staff-access-group'

     chnfsv4acl -R o=rwxdc,o=rwf,e=fd,g:staff-access-group=rwxfd

    To include Read-Write-Execute-FileInherit-DirectoryInherit for the group
    'staff-access-group' but only propogate to one subdirectories created
    below the top level.

     chnfsv4acl -R o=rwxdc,o=rwf,e=fd,g:staff-access-group=rwxfdp

    To explicitly set a DENY flag for everyone, rather than the default
    ALLOW,

     chnfsv4acl -R o=rwxdc,o=rwf,e=Dfd

    To add Read permissions to the existing permissions for everyone

     chnfsv4acl e+r <file>

    To append staff-access-group preserving the current ACL <file>,

     chnfsv4acl -p g:staff-access-group=rwxfdp <file>

    To remove all g:staff-access-group ACEs and replace with a single
    g:staff-access-group ACE

     chnfsv4acl -r g:staff-access-group=rwxfdp <file>

    To remove the ACE staff-access-group from the ACL

     chnfsv4acl g:staff-access-group# <file>

    To remove the 'c' flag from all ACEs

     chnfsv4acl a-c <file>

    to add the x flag to all ACEs

     chnfsv4acl +x <file>

    or

     chnfsv4acl a+x <file>

    To recursavely apply the ACL of an existing directory to other
    directories, without conscructing a MODE statement

     mmgetacl <directory> | chnfsv4acl -R - <target-directory>

    that is, the full ACL is read from the standard input

# SEE ALSO
    mmgetacl

# BUGS
    The '-' operation can have unexpected results to an ACE (because of the
    nonisomorphic mapping between MODE access flags and the 14 NFSv4 access
    flags)

    Mixing '+/-' and '=' in the same MODE can cause reordering of the ACEs.

# AUTHOR
    John Ireland - University of Edinburgh MRC IGMM

